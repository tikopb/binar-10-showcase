import Button from "../../Button/button.js";
import {useRouter} from 'next/router';

const List = () => {
    
    const router = useRouter();
    
    const startGame = () => {
        router.push('/gameprs');
    }
    
    return(
        <div className="content">
            <h1> Score </h1>
            <div className="contentList"> 
                <h1>Rock Papper Games </h1>
                <h3>Traditional game is use hand as gesture on gaming</h3>
                <Button onClick={startGame} title={"Play Game"} />
            </div>
            <div className="contentList"> 
                <div>    
                    <h1>Minecraft</h1>
                    <h3>Minecraft is a sandbox video game developed by the Swedish video game developer Mojang Studios</h3>
                    <Button onClick={startGame} title={"Play Game"} />
                </div>
                <div>
                    
                </div>
            </div>
            <div className="contentList"> 
                <div>
                    <h1>Tetris </h1>
                    <h3>Tetris, video game created by Russian designer Alexey Pajitnov in 1985 that allows players to rotate falling blocks strategically to clear levels</h3>
                    <Button onClick={startGame} title={"Play Game"} />
                </div>
                <div>
                    
                </div>

            </div>
            <div className="contentList"> 
                <div>    
                    <h1>Pac-Man</h1>
                    <h3>Pac-Man is an action maze chase video game; the player controls the eponymous character through an enclosed maze</h3>
                    <Button onClick={startGame} title={"Play Game"} />
                </div>
                <div>
                    
                </div>    
            </div>
        </div> 
    );
};



export default List;