import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPlayer, updateplayer } from '../../../../redux/action';
import swal from 'sweetalert';

import Input from '../../UI/Input';
import Button from '../../UI/Button';
import classes from './ProfileDetail.module.css';

const ProfileDetail = () => {
  const dispatch = useDispatch();
  const { user, playerData } = useSelector(state => state.reducUser);

  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [city, setCity] = useState('');
  const [bio, setBio] = useState('');

  const { uid } = user;

  useEffect(() => {
    dispatch(fetchPlayer(uid));
  }, [dispatch, uid]);

  useEffect(() => {
    setName(playerData?.name);
    setUsername(playerData?.username);
    setEmail(playerData?.email);
    setCity(playerData?.city);
    setBio(playerData?.bio);
  }, [playerData]);

  const forName = e => {
    e.preventDefault();
    setName(e.target.value);
  };

  const forUsername = e => {
    e.preventDefault();
    setUsername(e.target.value);
  };

  const forEmail = e => {
    e.preventDefault();
    setEmail(e.target.value);
  };

  const forCity = e => {
    e.preventDefault();
    setCity(e.target.value);
  };

  const forBio = e => {
    e.preventDefault();
    setBio(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    const payload = {
      name,
      username,
      email,
      city,
      bio,
    };

    swal({
      title: 'Your profile is updated',
      icon: 'success',
      buttons: 'OK!',
    })
      .then(willUpdate => {
        if (willUpdate) {
          dispatch(updateplayer(uid, payload));
          dispatch(fetchPlayer(uid));
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <form className={`${'col'} ${classes.form}`} onSubmit={handleSubmit}>
      <h2>User Data</h2>
      <hr />
      <Input
        label="Full Name"
        for="fullname"
        placeholder="Full Name"
        type="text"
        value={name}
        onChange={forName}
      />
      <Input
        label="Username"
        for="username"
        placeholder="Username"
        type="text"
        value={username}
        onChange={forUsername}
      />
      <Input
        label="Email"
        for="email"
        placeholder="example@xyz.com"
        type="email"
        value={email}
        onChange={forEmail}
      />
      <Input
        label="City"
        for="city"
        placeholder="City"
        type="text"
        value={city}
        onChange={forCity}
      />
      <Input
        label="Bio"
        for="bio"
        placeholder="Bio"
        type="text"
        value={bio}
        onChange={forBio}
      />
      <div style={{ textAlign: 'center' }}>
        <Button type="submit" title="Update" />
      </div>
    </form>
  );
};

export default ProfileDetail;

// const mapStateToProps = state => {
//   return {
//     isLogin: state.reducUser.isLogin,
//     user: state.reducUser.user,
//     playerData: state.reducUser.playerData,
//   };
// };

// export default connect(mapStateToProps, { fetchPlayer, updateplayer })(
//   ProfileDetail
// );
