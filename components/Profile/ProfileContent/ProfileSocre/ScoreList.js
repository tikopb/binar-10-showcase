import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchPlayer } from '../../../../redux/action';

import classes from './ScoreList.module.css';

const ScoreList = props => {
  useEffect(() => {
    props.fetchPlayer(props.user.uid);
  }, []);

  return (
    <React.Fragment>
      <li className={classes.list}>
        <h6>Play</h6>
        {props.playerData.play}
      </li>
      <li className={classes.list}>
        <h6>Score</h6>
        {props.playerData.score}
      </li>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    isLogin: state.reducUser.isLogin,
    user: state.reducUser.user,
    playerData: state.reducUser.playerData,
  };
};

export default connect(mapStateToProps, { fetchPlayer })(ScoreList);
