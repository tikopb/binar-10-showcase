import React from 'react';

import classes from './Input.module.css';

const Input = props => {
  return (
    <div className={classes.control}>
      <label htmlFor={props.for}>{props.label}</label>
      <input
        type={props.type}
        value={props.value}
        onChange={props.onChange}
        defaultValue={props.defaultValue}
        placeholder={props.placeholder}
      />
    </div>
  );
};

export default Input;
