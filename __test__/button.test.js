/**
 * @jest-environment jsdom
 */

import React from 'react';
import {render} from '@testing-library/react';
import Button from '../components/Button/button.js';

it('should have button-rergister className', () => {
    const {container, getByText} = render(<Button />);

    expect(container.querySelector('.button-register')).toBeTruthy();
    expect(getByText).toBeTruthy();
});