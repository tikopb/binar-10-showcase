import Navbar from '../components/navbar';
import BodyLogin from '../components/bodyLogin'

export default function Login() {
    return(
        <>
            <Navbar />
            <BodyLogin />
        </>
    )
}